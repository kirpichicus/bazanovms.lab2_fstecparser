﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public class Threat
    {
        public int Id { get; private set; }
        public string ThreatName { get; set; }
        public string ThreatDescription { get; set; }
        public string ThreatSource { get; set; }
        public string ThreatObject { get; set; }
        public bool Сonfidentiality { get; set; }
        public bool Integrity { get; set; }
        public bool Availability { get; set; }

        public Threat(int id, string threatName, string threatDescription, string threatSource,
            string threatObject, string сonfidentiality, string integrity, string availability)
        {
            Id = id;
            ThreatName = threatName;
            ThreatDescription = threatDescription;
            ThreatSource = threatSource;
            ThreatObject = threatObject;
            if (сonfidentiality == "1"|| сonfidentiality =="True") Сonfidentiality = true;
            else Сonfidentiality = false;
            if (integrity == "1"||integrity=="True") Integrity = true;
            else Integrity = false;
            if (availability == "1"||availability=="True") Availability = true;
            else Availability = false;
        }

        public override bool Equals(object obj)
        {
            var threat = obj as Threat;
            return threat != null &&
                   Id == threat.Id &&
                   ThreatName == threat.ThreatName &&
                   ThreatDescription == threat.ThreatDescription &&
                   ThreatSource == threat.ThreatSource &&
                   ThreatObject == threat.ThreatObject &&
                   Сonfidentiality == threat.Сonfidentiality &&
                   Integrity == threat.Integrity &&
                   Availability == threat.Availability;
        }
        public override string ToString()
        {
            return this.Id + this.ThreatName;
        }
        public BindingList<string> fullInfo()
        {
            BindingList<string> ret = new BindingList<string>();
            ret.Add("Id: "+this.Id.ToString());
            ret.Add("Название: "+this.ThreatName.ToString());
            ret.Add("Описание "+this.ThreatDescription.ToString());
            ret.Add("Источник "+this.ThreatSource.ToString());
            ret.Add("Объект: "+this.ThreatObject.ToString());
            ret.Add("Конфиденциальность "+this.Сonfidentiality.ToString());
            ret.Add("Целостность: "+this.Integrity.ToString());
            ret.Add("Доступность: "+this.Availability.ToString());
            return ret;
        }
        public override int GetHashCode()
        {
            var hashCode = 1156073086;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ThreatName);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ThreatDescription);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ThreatSource);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ThreatObject);
            hashCode = hashCode * -1521134295 + Сonfidentiality.GetHashCode();
            hashCode = hashCode * -1521134295 + Integrity.GetHashCode();
            hashCode = hashCode * -1521134295 + Availability.GetHashCode();
            return hashCode;
        }
    }
}