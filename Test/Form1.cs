﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Windows.Forms;
using OfficeOpenXml;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public partial class Form1 : Form
    {
        private static int totalRecords = 0;
        private static int pageSize = 15;
        public static BindingList<Threat> threatList = new BindingList<Threat>();
        public static BindingList<string> t1 = new BindingList<string>();
        public static BindingList<string> t2 = new BindingList<string>();
        public static BindingList<string> t3 = new BindingList<string>();
        public static BindingList<string> t4 = new BindingList<string>();
        public static BindingList<string> t5 = new BindingList<string>();
        public static BindingList<string> t6 = new BindingList<string>();
        public static BindingList<string> t7 = new BindingList<string>();
        public static BindingList<string> t8 = new BindingList<string>();
        public static string filenametxt = "threat.txt";
        public static string filenamexlsx = "threat.xlsx";

        public Form1()
        {
            InitializeComponent();
            ParseLocalBase(filenametxt);
            listBox3.MeasureItem += lst_MeasureItem;
            listBox3.DrawItem += lst_DrawItem;
            bindingNavigator1.BindingSource = bindingSource1;
            bindingSource1.CurrentChanged += new System.EventHandler(bindingSource1_CurrentChanged);
            bindingSource1.DataSource = new PageOffsetList();
        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {
            // The desired page has changed, so fetch the page of records using the "Current" offset 
            try
            {
                int offset = ((Threat)bindingSource1.Current).Id - 1;
                var records = new BindingList<Threat>();
                for (int i = offset; i < offset + pageSize && i < threatList.Count; i++)
                {
                    records.Add(threatList[i]);
                }
                dataGridView1.DataSource = records;
            }
            catch (Exception)
            {
                
            }
            
        }

        class PageOffsetList : System.ComponentModel.IListSource
        {
            public bool ContainsListCollection { get; protected set; }

            public System.Collections.IList GetList()
            {
                // Return a list of page offsets based on "totalRecords" and "pageSize"
                var pageOffsets = new BindingList<Threat>();
                for (int offset = 0; offset < threatList.Count; offset += pageSize)
                {
                    pageOffsets.Add(threatList[offset]);
                }

                return pageOffsets;
            }
        }
        // Загрузить
        private void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                new WebClient().DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx", filenamexlsx);
                MessageBox.Show("Файл с угрозами получен");
            }
            catch (Exception)
            {
                MessageBox.Show("Невозможно получить файл с угрозами");
            }
        }
        //Парсить таблицу с Threat
        private static void ThreatParse2(string fileName)
        {
            try
            {
                DateTime start = DateTime.Now;
                threatList.Clear();
                using (ExcelPackage xlPackage = new ExcelPackage(new FileInfo(fileName)))
                {
                    var myWorksheet = xlPackage.Workbook.Worksheets.First();
                    var totalRows = myWorksheet.Dimension.End.Row;
                    var totalColumns = myWorksheet.Dimension.End.Column;

                    for (int rowNum = 3; rowNum <= totalRows; rowNum++)
                    {
                        var row = myWorksheet.Cells[rowNum, 1, rowNum, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString());
                        string stmp = string.Join("@", row);
                        stmp = String.Join("", stmp.Split('\n', '\0', '\r'));
                        string[] tmp = string.Join("@", stmp).Split('@');
                        threatList.Add(new Threat(int.Parse(tmp[0]), tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6], tmp[7]));
                    }

                }
                DateTime end = DateTime.Now;
                MessageBox.Show("Данные загружены");
            }
            catch (Exception)
            {
                MessageBox.Show("Файл не найден, скачайте его");
            }
            
        }

        private void btnParse_Click(object sender, EventArgs e)
        {
            ThreatParse2(filenamexlsx);
            bindingSource1.DataSource = new PageOffsetList();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ParseLocalBase(filenametxt);
            bindingSource1.DataSource = null;
            bindingSource1.DataSource = new PageOffsetList();
        }
        private void ParseLocalBase(string filename)
        {
            try
            {
                threatList.Clear();
                if (!File.Exists(filename))
                {
                    MessageBox.Show("Локальная база данных с угрозами не найдена, \nпопробуйте загрузить файл с сайта ФСТЭК и извлечь из него информацию");
                    return;
                }
                else
                {
                    string[] s = File.ReadAllLines(filename);
                    if (s.Length < 1)
                    {
                        MessageBox.Show("Локальная база пуста");
                        return;
                    }
                    for (int i = 0; i < s.Length; i++)
                    {
                        string[] tmp = s[i].Split('@');
                        threatList.Add(new Threat(int.Parse(tmp[0]), tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6], tmp[7]));
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка загрузки из локальной базы");
                return;
            }
            MessageBox.Show("Данные загружены");
        }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {

        }

        private void bindingSource1_CurrentChanged_1(object sender, EventArgs e)
        {

        }

        private void bindingSource1_ListChanged(object sender, ListChangedEventArgs e)
        {

        }

        private void файлToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        private static void update(string fileName)
        {
            BindingList<Threat> Was = new BindingList<Threat>();
            BindingList<Threat> Is = new BindingList<Threat>();
            string s;
            int updatedThreats = 0;
            int addedThreats = 0;
            DateTime start = DateTime.Now;
            try
            {
                try
                {
                    new WebClient().DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx", filenamexlsx);
                    MessageBox.Show("Файл с угрозами получен");
                }
                catch (Exception)
                {
                    MessageBox.Show("Невозможно получить файл с угрозами");
                }
                using (ExcelPackage xlPackage = new ExcelPackage(new FileInfo(fileName)))
                {
                    var myWorksheet = xlPackage.Workbook.Worksheets.First();
                    var totalRows = myWorksheet.Dimension.End.Row;
                    var totalColumns = myWorksheet.Dimension.End.Column;

                    for (int rowNum = 3; rowNum <= totalRows; rowNum++)
                    {
                        var row = myWorksheet.Cells[rowNum, 1, rowNum, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString());
                        string stmp = string.Join("@", row);
                        stmp = String.Join("", stmp.Split('\n', '\0', '\r'));
                        string[] tmp = string.Join("@", stmp).Split('@');
                        Threat tmpThreat = new Threat(int.Parse(tmp[0]), tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6], tmp[7]);
                        if (int.Parse(tmp[0]) > threatList.Count)
                        {
                            MessageBox.Show("Добавлена запись\n"+tmpThreat.ToString());
                            threatList.Add(tmpThreat);
                            addedThreats++;
                        }
                        else if (!threatList[int.Parse(tmp[0]) - 1].Equals(tmpThreat))
                        {
                            Was.Add(threatList[int.Parse(tmp[0]) - 1]);
                            Is.Add(tmpThreat);
                            threatList[int.Parse(tmp[0]) - 1] = tmpThreat;
                            updatedThreats++;
                        }
                    }

                }

            }
            catch (Exception x)
            {
                MessageBox.Show("Ошибка обновления");
                return;
            }

            DateTime end = DateTime.Now;
            MessageBox.Show("Успешное обновление!\nВремя выполнения    - " + (end - start).TotalSeconds.ToString() + "\nОбновленно записей - " + updatedThreats + "\nДобавленно записей  - " + addedThreats);
            if (updatedThreats == 0) return;
            for (int i = 0; i < Was.Count; i++)
            {
                MessageBox.Show(CompareThreat(Was, Is,i));
            }
        }
        private static string CompareThreat(BindingList<Threat> Was, BindingList<Threat> Is,int i)
        {

            string s = "";
            s +="Id - " + Was[i].Id+"\n";
                if (!Was[i].Id.Equals(Is[i].Id)) s += "ID было - \"" + Was[i].Id + "\", стало -  \"" + Is[i].Id + "\"\n";
                if (!Was[i].ThreatName.Equals(Is[i].ThreatName)) s += "Имя угрозы было - \"" + Was[i].ThreatName + "\", стало -  \"" + Is[i].ThreatName + "\"\n";
                if (!Was[i].ThreatDescription.Equals(Is[i].ThreatDescription)) s += "Описание угрозы было - \"" + Was[i].ThreatDescription + "\", стало -  \"" + Is[i].ThreatDescription + "\"\n";
                if (!Was[i].ThreatSource.Equals(Is[i].ThreatSource)) s += "Источник угрозы было - \"" + Was[i].ThreatSource + "\", стало -  \"" + Is[i].ThreatSource + "\"\n";
                if (!Was[i].ThreatObject.Equals(Is[i].ThreatObject)) s += "Объект угрозы было - \"" + Was[i].ThreatObject + "\", стало -  \"" + Is[i].ThreatObject + "\"\n";
                if (!Was[i].Сonfidentiality.Equals(Is[i].Сonfidentiality)) s += "Конфиденциальность было - \"" + Was[i].Сonfidentiality + "\", стало -  \"" + Is[i].Сonfidentiality + "\"\n";
                if (!Was[i].Integrity.Equals(Is[i].Integrity)) s += "Целостность было - \"" + Was[i].Integrity + "\", стало -  \"" + Is[i].Integrity + "\"\n";
                if (!Was[i].Availability.Equals(Is[i].Availability)) s += "Доступность было - \"" + Was[i].Availability + "\", стало -  \"" + Is[i].Availability + "\"\n";

            return s;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void обновитьToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
                update(filenamexlsx);
                bindingSource1.DataSource = new PageOffsetList();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            pageSize = (int)numericUpDown1.Value;
            bindingSource1.DataSource = new PageOffsetList();
        }

        private void numericUpDown1_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            MessageBox.Show(((Threat)bindingSource1.Current).Id.ToString());
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int num =int.Parse( dataGridView1.CurrentRow.Index.ToString());
            listBox1.DataSource = new BindingList<string> { threatList[num].Id.ToString() };
            listBox2.DataSource = new BindingList<string> { threatList[num].ThreatName };
            listBox3.DataSource = new BindingList<string> { threatList[num].ThreatDescription };
            listBox4.DataSource = new BindingList<string> { threatList[num].ThreatSource };
            listBox5.DataSource = new BindingList<string> { threatList[num].ThreatObject };
            listBox6.DataSource = new BindingList<string> { threatList[num].Сonfidentiality.ToString() };
            listBox7.DataSource = new BindingList<string> { threatList[num].Integrity.ToString()};
            listBox8.DataSource = new BindingList<string> { threatList[num].Availability.ToString() };
            
 
        }

        private void lst_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            e.ItemHeight = (int)e.Graphics.MeasureString(listBox3.Items[e.Index].ToString(), listBox3.Font, listBox3.Width).Height;
        }

        private void lst_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (listBox3.Items.Count > 0)
            {
                e.DrawBackground();
                e.DrawFocusRectangle();
                e.Graphics.DrawString(listBox3.Items[e.Index].ToString(), e.Font, new SolidBrush(e.ForeColor), e.Bounds);
            }
        }

        private void listBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> tmp = new List<string>();
                string s="";
                foreach (var item in threatList)
                {
                    s = s + item.Id + "@" + item.ThreatName + "@" + item.ThreatDescription + "@" + item.ThreatSource + "@" + item.ThreatObject + "@" + item.Сonfidentiality + "@" + item.Integrity + "@" + item.Availability;
                    tmp.Add(s);
                    s = "";
                }
                File.WriteAllLines(filenametxt, tmp);
                MessageBox.Show("Файл сохранен");
            }
            catch (Exception x)
            {
                MessageBox.Show("Ошибка сохранения файла");
            }
        }



        /*private void threatsOnPage_ValueChanged(object sender, EventArgs e)
        {
            pageSize = (int)threatsOnPage.Value;
            bindingSource1.DataSource = new PageOffsetList();
        }*/
    }
}